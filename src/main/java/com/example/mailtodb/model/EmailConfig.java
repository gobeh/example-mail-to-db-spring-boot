package com.example.mailtodb.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "spring.mail")
public class EmailConfig {
	private String host;
	private Integer port;
	private String username;
	private String password; 
	private String protocol;
}
