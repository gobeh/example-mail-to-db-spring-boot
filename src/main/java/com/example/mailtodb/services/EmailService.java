package com.example.mailtodb.services;


import com.example.mailtodb.constant.AppConstant;
import com.example.mailtodb.dao.AttachmentDao;
import com.example.mailtodb.dao.EmailDataDao;
import com.example.mailtodb.dto.AttachmentDto;
import com.example.mailtodb.entity.Attachment;
import com.example.mailtodb.entity.EmailData;
import com.example.mailtodb.model.EmailConfig;
import com.example.mailtodb.util.MessageParser;
import io.minio.errors.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Service
public class EmailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    @Autowired
    private EmailConfig emailConfig;

    @Autowired
    private EmailDataDao emailDataDao;

    @Autowired
    private AttachmentDao attachmentDao;

    @Autowired
    private MessageParser messageParser;

    @Autowired
    private MinioAdapter minioAdapter;

    Folder emailFolder;
    Store store;
    Properties properties = new Properties();

    @PostConstruct
    void setup() {
        String server = emailConfig.getHost();
        properties.put("mail.pop3.host", server);
        properties.put("mail.pop3.port", emailConfig.getPort());
        //properties.put("mail.pop3.starttls.enable", "true");
        Session emailSession = Session.getDefaultInstance(properties);
        Store store = null;
        try {
            store = emailSession.getStore("pop3s");
            store.connect(server, emailConfig.getUsername(), emailConfig.getPassword());
            emailFolder = store.getFolder("INBOX");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(fixedRate = 5000)
    synchronized void read() throws MessagingException, IOException, NoSuchAlgorithmException {

        emailFolder.open(Folder.READ_ONLY);
        Message[] messages = emailFolder.getMessages();

        LOGGER.info("jumlah email baru {}", messages.length);
        for (Message message : messages) {
            saveToDb(message);
            //System.out.println("Sukses tersimpan");
        }
        emailFolder.close();
    }

    private void saveToDb(Message message) throws MessagingException, IOException, NoSuchAlgorithmException {

        EmailData emailData = new EmailData();

        String[] senderInfo = getSenderInfo(message.getFrom());
        emailData.setSender(senderInfo[0]);
        emailData.setSenderName(senderInfo[1]);
        emailData.setReciever(emailConfig.getUsername());
        emailData.setContent(message.getContent() == null ? "" : messageParser.getTextFromMessage(message));
        emailData.setSubject(message.getSubject());
        emailData.setSentDate(message.getSentDate());

        String stringToHash = Arrays.toString(message.getFrom()) + (message.getSubject() == null ? "" : message.getSubject())
                + emailConfig.getUsername();
        emailData.setHash(hashContent(stringToHash));

        try {
            //save email
            emailDataDao.save(emailData);
            //handling attachment
            if (message.isMimeType(("multipart/*"))) {
                //list attachment
                List<String> urls = messageParser.getFileLinksFromMimeMultipart((MimeMultipart) message.getContent());
                //save attachment
                if (urls.size() != 0) {
                    saveAttachment(emailData, urls);
                }
            }
            LOGGER.info("email sukses tersimpan");
        } catch (Exception e) {
            LOGGER.info("Error menyimpan: ", e);
        }
    }

    private void saveAttachment(EmailData emailData, List<String> urls) {
        if (urls.size() != 0) {
            for (String url : urls) {
                Attachment attachment = new Attachment();

                String[] content = url.split(";");
                attachment.setUrl(content[0]); //url link
                attachment.setCid(content[1].equals("null") ? null : content[1]); //cid
                attachment.setEmailData(emailData);
                attachment.setTipe(content[2]); //type attachment

                attachmentDao.save(attachment);
                LOGGER.info(content[2] + " sukses tersimpan");
            }
        }
    }

    private String hashContent(String content) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(content.getBytes());
        byte[] digest = md.digest();

        return DatatypeConverter
                .printHexBinary(digest).toUpperCase();
    }

//    private Long checkData(Message message) throws MessagingException, IOException, NoSuchAlgorithmException {
//        String stringToHash = Arrays.toString(message.getFrom()) + (message.getSubject() == null ? "" : message.getSubject())
//                + emailConfig.getUsername() + (message.getContent() == null ? "" : MessageParser.getTextFromMessage(message));
//
//        return emailDataDao.countByHashEquals(hashContent(stringToHash));
//    }

    public String myAccount(String id) {
        Optional<EmailData> emailData = emailDataDao.findById(id);
        if (!emailData.isPresent())
            return "";

        return emailData.get().getReciever();
    }

    private String[] getSenderInfo(Address[] address) {
        if (address == null)
            return null;

        return new String[]{((InternetAddress) address[0]).getAddress(), ((InternetAddress) address[0]).getPersonal()};
    }

    public List<AttachmentDto> getListRegularAttachment(String emailId) throws IOException, InvalidResponseException, InvalidKeyException,
            NoSuchAlgorithmException, ServerException, ErrorResponseException, XmlParserException, InsufficientDataException, InternalException {

        List<Attachment> attachmentList = attachmentDao.findAllByEmailDataIdAndTipe(emailId, AppConstant.REGULAR_ATTACHMENT);
        if (attachmentList.size() != 0) {
            List<AttachmentDto> attachmentDtoList = new LinkedList<>();
            for (Attachment attachment : attachmentList) {
                AttachmentDto attachmentDto = new AttachmentDto();

                String[] infoObject = minioAdapter.getFileInfo(attachment.getUrl());

                attachmentDto.setUrl(minioAdapter.getUrlFile(attachment.getUrl()));
                attachmentDto.setName(infoObject[0]);
                attachmentDto.setSize(infoObject[1]);

                attachmentDtoList.add(attachmentDto);
            }

            return attachmentDtoList;

        } else
            return null;
    }

}
