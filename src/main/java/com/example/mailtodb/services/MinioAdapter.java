package com.example.mailtodb.services;

import io.minio.*;
import io.minio.errors.*;
import io.minio.http.Method;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@Service
public class MinioAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(MinioAdapter.class);

    @Autowired
    private MinioClient minioClient;

    @Value("${minio.buckek.name}")
    String defaultBucketName;


    public String uploadFile(String name, byte[] content, String tipe) {
        LocalDateTime tanggal = LocalDateTime.now();
        String folder = tipe + "/" + tanggal.getYear() + "/" + tanggal.getMonthValue() + "/" + tanggal.getDayOfMonth();

        File file = new File(name);
        file.canWrite();
        file.canRead();
        try {
            FileOutputStream iofs = new FileOutputStream(file);
            iofs.write(content);

            minioClient.uploadObject(
                    UploadObjectArgs.builder()
                            .bucket(defaultBucketName)
                            .object(folder + "/" + name)
                            .filename(file.getAbsolutePath())
                            .build()
            );
        } catch (MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }

        return folder + "/" + name;
    }

    public byte[] getFile(String file) {
        try {
            InputStream obj = minioClient.getObject(
                    GetObjectArgs.builder()
                            .bucket(defaultBucketName)
                            .object(file)
                            .build()
            );

            byte[] content = IOUtils.toByteArray(obj);
            obj.close();

            return content;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getUrlFile(String objectName) throws IOException, InvalidKeyException,
            InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException,
            ServerException, InternalException, XmlParserException, ErrorResponseException {

        return minioClient.getPresignedObjectUrl(
                GetPresignedObjectUrlArgs.builder()
                        .bucket(defaultBucketName)
                        .object(objectName)
                        .method(Method.GET)
                        .expiry(2, TimeUnit.HOURS)
                        .build());
    }

    public String[] getFileInfo(String objectName) throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException,
            NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {

        StatObjectResponse stat =
                minioClient.statObject(
                        StatObjectArgs.builder().bucket(defaultBucketName).object(objectName).build());

        String[] pathName = stat.object().split("/");

        String[] infoObject = new String[2];
        infoObject[0] = pathName[pathName.length - 1]; //name
        infoObject[1] = FileUtils.byteCountToDisplaySize(stat.size()); //size

        return infoObject;
    }

    public void removeObject(String file) {
        try {
            minioClient.removeObject(RemoveObjectArgs.builder()
                    .bucket(defaultBucketName)
                    .object(file)
                    .build());
        } catch (Exception e) {
            LOGGER.info("error: {}", e);
        }
    }

}
