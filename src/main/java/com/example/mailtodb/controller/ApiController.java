package com.example.mailtodb.controller;

import com.example.mailtodb.constant.AppConstant;
import com.example.mailtodb.dao.AttachmentDao;
import com.example.mailtodb.dto.AttachmentDto;
import com.example.mailtodb.entity.Attachment;
import com.example.mailtodb.services.MinioAdapter;
import io.minio.errors.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    private AttachmentDao attachmentDao;

    @Autowired
    private MinioAdapter minioAdapter;

    @GetMapping("/attachment")
    public List<Attachment> getAllAttachmentByEmailIdAndCid(@RequestParam(value = "id", required = false) String id,
                                                            @RequestParam(value = "cid", required = false) String cid) {
        LOGGER.info("id: {}", id);
        if (id == null) {
            return null;
        } else {
            return attachmentDao.findAllByEmailDataIdAndCidAndTipe(id, cid, AppConstant.INLINE_ATTACHMENT);
        }
    }

    @GetMapping("/inline-attachment")
    public List<AttachmentDto> getAllAttachmentByEmailId(@RequestParam(value = "id", required = false) String id
    ) throws IOException, InvalidResponseException, InvalidKeyException, NoSuchAlgorithmException,
            ServerException, ErrorResponseException, XmlParserException, InsufficientDataException, InternalException {

        LOGGER.info("id: {}", id);
        if (id == null) {
            return null;
        } else {
            List<AttachmentDto> attachmentDtoList = new LinkedList<>();

            List<Attachment> attachmentList = attachmentDao.findAllByEmailDataIdAndCidIsNotNullAndTipe(id, AppConstant.INLINE_ATTACHMENT);
            for (Attachment attachment : attachmentList) {
                AttachmentDto attachmentDto = new AttachmentDto();

                attachmentDto.setCid(attachment.getCid());
                attachmentDto.setUrl(minioAdapter.getUrlFile(attachment.getUrl()));

                attachmentDtoList.add(attachmentDto);
            }

            return attachmentDtoList;
        }
    }
}
