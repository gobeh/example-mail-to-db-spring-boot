package com.example.mailtodb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping(value = {"/", "/index"})
    public String login(ModelMap mm) {

        mm.addAttribute("error", true);
        return "redirect:/inbox";
    }
}
