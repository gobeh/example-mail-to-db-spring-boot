package com.example.mailtodb.controller;

import com.example.mailtodb.dao.EmailDataDao;
import com.example.mailtodb.entity.EmailData;
import com.example.mailtodb.model.EmailConfig;
import com.example.mailtodb.services.EmailService;
import io.minio.errors.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

@Controller
public class InboxController {

    @Autowired
    private EmailConfig emailConfig;

    @Autowired
    private EmailDataDao emailDataDao;

    @Autowired
    private EmailService emailService;

    @RequestMapping(value = {"/inbox"})
    public String inboxPage(ModelMap mm, @PageableDefault(size = 14) Pageable page) {

        Page<EmailData> result = emailDataDao.findAllByOrderBySentDateDesc(page);

        mm.addAttribute("data", result);
        mm.addAttribute("myAccount", emailConfig.getUsername());

        return "inbox";
    }

    @GetMapping("/inbox/view")
    public String viewMail(@RequestParam String id, ModelMap mm) throws IOException, InvalidResponseException, InvalidKeyException, NoSuchAlgorithmException,
            ServerException, InternalException, XmlParserException, InsufficientDataException, ErrorResponseException {

        Optional<EmailData> emailData = emailDataDao.findById(id);

        mm.addAttribute("emailData", emailData.get());
        mm.addAttribute("myAccount", emailConfig.getUsername());
        mm.addAttribute("listAttachment", emailService.getListRegularAttachment(id));

        return "detail-email";
    }
}
