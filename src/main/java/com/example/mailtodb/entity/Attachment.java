package com.example.mailtodb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "public", name = "attachment")
public class Attachment {
        @Id
        @GeneratedValue(generator = "system-uuid")
        @GenericGenerator(name = "system-uuid", strategy = "uuid2")
        @Column(name = "id", length = 36)
        private String id;

        @Column(name = "cid")
        private String cid;

        @ManyToOne
        @JoinColumn(name = "id_email_data")
        private EmailData emailData;

        @Column(name = "url")
        private String url;

        @Column(name = "tipe")
        private String tipe;
}
