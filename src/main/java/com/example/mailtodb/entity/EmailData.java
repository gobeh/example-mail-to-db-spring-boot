package com.example.mailtodb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;


@Data
@Entity
@Table(schema = "public", name = "email_data")
public class EmailData {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "id", length = 36)
    private String id;

    @Column(name = "subject")
    private String subject;

    //email
    @Column(name = "sender")
    private String sender;

    @Column(name = "sender_name")
    private String senderName;

    @Column(name = "reciever")
    private String reciever;

    @Column(name = "content", columnDefinition="TEXT")
    private String content;

    @Column(name = "sent_date")
    private Date sentDate;

    @Column(name = "hash")
    private String hash;
}
