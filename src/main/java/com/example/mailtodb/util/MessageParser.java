package com.example.mailtodb.util;

import com.example.mailtodb.constant.AppConstant;
import com.example.mailtodb.services.MinioAdapter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

@Service
public class MessageParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageParser.class);

    @Autowired
    private MinioAdapter minioAdapter;

    public String getTextFromMessage(Message message) throws MessagingException, IOException {

        String result = "";

        if (message.isMimeType("text/plain")) {
            result = message.getContent().toString();
        } else if (message.isMimeType("text/html")) { // **
            result = message.getContent().toString(); // **
        } else if (message.isMimeType("multipart/*")) {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            result = getTextFromMimeMultipart(mimeMultipart);
        }
        return result;
    }

    private static String getTextFromMimeMultipart(
            MimeMultipart mimeMultipart) throws MessagingException, IOException {

        StringBuilder result = new StringBuilder();
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/html")) {
                result = new StringBuilder(IOUtils.toString(
                        MimeUtility.decode(bodyPart.getInputStream(), "quoted-printable"), Charset.defaultCharset()
                ));
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                result.append(getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent()));
            }
        }
        return result.toString();
    }


    public List<String> getFileLinksFromMimeMultipart(
            MimeMultipart mimeMultipart) throws MessagingException, IOException {

        List<String> linksFiles = new LinkedList<>();

        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.getContentType().contains("image/") || bodyPart.getContentType().contains("application/")
                    || bodyPart.getContentType().contains("text/plain")) {

                //check attachment
                if (bodyPart.getDisposition() != null) {
                    System.out.println("content type" + bodyPart.getContentType());
                    byte[] byteArray = IOUtils.toByteArray(bodyPart.getInputStream());
                    String nameFile = bodyPart.getFileName().replaceAll("/|\\?|=", "");
                    String[] extension = bodyPart.getContentType().split("/|;");
                    //get cid from header
                    String cid = getCidFromHeader(bodyPart.getAllHeaders());
                    //handle base64 filename
                    if (nameFile.contains("UTF-8b")) {
                        nameFile = getStringFromBase64(nameFile.replace("UTF-8b", ""));
                    }
                    if (!nameFile.contains("."))
                        nameFile = nameFile + "." + extension[1];

                    String url = minioAdapter.uploadFile(nameFile, byteArray, "attachment");

                    //type attachment
                    String tipe = (bodyPart.getDisposition().equals("inline") ? AppConstant.INLINE_ATTACHMENT : AppConstant.REGULAR_ATTACHMENT);

                    linksFiles.add(url + ";" + cid + ";" + tipe);
                }
            } else if (bodyPart.getContentType().contains("multipart/")) {

                linksFiles.addAll(getFileLinksFromMimeMultipart((MimeMultipart) bodyPart.getContent()));
            }
        }
        return linksFiles;
    }

    public String[] getCidFromContent(String content) throws MessagingException {
        return StringUtils.substringsBetween(content, "cid:", "\"");
    }

    private static String getStringFromBase64(String encodedString) {
        byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
        return new String(decodedBytes);
    }

    private static String getCidFromHeader(Enumeration<Header> headers) {
        ArrayList<Header> list = Collections.list(headers);
        String result = null;
        for (Header h : list) {
            if (h.getName().equals("Content-ID")) {
                result = h.getValue().replaceAll("<|>", "");
            }
        }
        return result;
    }

}
