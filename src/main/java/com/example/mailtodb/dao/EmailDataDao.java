package com.example.mailtodb.dao;

import com.example.mailtodb.entity.EmailData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmailDataDao extends PagingAndSortingRepository<EmailData, String> {
    Long countByHashEquals(String hash);

    Page<EmailData> findAllByOrderByIdDesc(Pageable page);

    Page<EmailData> findAllByOrderBySentDateDesc(Pageable page);
}
