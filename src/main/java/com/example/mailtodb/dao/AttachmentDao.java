package com.example.mailtodb.dao;

import com.example.mailtodb.entity.Attachment;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AttachmentDao extends PagingAndSortingRepository<Attachment, String> {
    List<Attachment> findAllByEmailDataIdAndCidAndTipe(String id, String cid, String tipe);

    List<Attachment> findAllByEmailDataIdAndCidIsNotNullAndTipe(String id, String tipe);

    List<Attachment> findAllByEmailDataIdAndCidIsNullAndTipe(String id, String tipe);

    List<Attachment> findAllByEmailDataIdAndTipe(String id, String tipe);
}
