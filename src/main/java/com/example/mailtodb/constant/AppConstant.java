package com.example.mailtodb.constant;

public interface AppConstant {
    final String INLINE_ATTACHMENT = "inline-attachment";
    final String REGULAR_ATTACHMENT = "regular-attachment";
}
